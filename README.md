# taartpuntenbot

This is an telegram bot application for taartpunten of the board of via.

### Set-up with Docker
```
docker build --tag taartbot .
docker run --name taartbot_1 taartbot
```

### Running
```
docker start taartbot_1
```

### Stopping the application
```
docker stop taartbot_1
```