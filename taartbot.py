#!/usr/bin/python3

import logging
import datetime
import sqlite3
import os

from telegram.ext import Updater, CommandHandler
from telegram.ext.jobqueue import Days
from dotenv import load_dotenv

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG
)


logger = logging.getLogger(__name__)


class TaartPuntenBot:
    def __init__(self, token):
        self.token = token
        self.updater = Updater(token, use_context=True)
        self.job_queue = self.updater.job_queue
        self.dispatcher = self.updater.dispatcher
        self.dispatcher.add_handler(CommandHandler("start", self.help))
        self.dispatcher.add_handler(CommandHandler("help", self.help))
        self.dispatcher.add_handler(CommandHandler("add", self.add_board_member))
        self.dispatcher.add_handler(CommandHandler("remove", self.remove_board_member))
        self.dispatcher.add_handler(CommandHandler("boardmembers", self.list_board_members))
        self.dispatcher.add_handler(CommandHandler("taartpunt", self.add_taartpunt))
        self.dispatcher.add_handler(CommandHandler("taart", self.remove_taartpunt))
        self.dispatcher.add_handler(CommandHandler("overview", self.overview))
        self.dispatcher.add_handler(CommandHandler("notify", self.notification, pass_job_queue=True))
        self.database_cursor = None
        self.database_conn = None
        self.setup_database()

    def setup_database(self):
        self.database_conn = sqlite3.connect("database.db", check_same_thread=False)
        self.database_cursor = self.database_conn.cursor()

    def help(self, update, _):
        help_text = """/add - Add a board member to the board member list
/remove - Remove a board member to the board member list
/boardmembers - See all board members
/taartpunt - Give a taartpunt to a board member
/taart - Remove taartpunten of a board member
/overview - View this weeks taartpunten score
/help - show this message
/notify - Enable BV notification"""
        update.message.reply_text(help_text)

    def remove_board_member(self, update, context):
        if context.args == []:
            update.message.reply_text("Usage: /remove 'name'")

        name = context.args[0].lower()
        self.database_cursor.execute("SELECT * FROM taartpunt WHERE username = (?)", (name,))
        found_board_member = self.database_cursor.fetchone()

        if found_board_member is None:
            update.message.reply_text("Board member not found")
        else:
            self.database_cursor.execute("DELETE FROM taartpunt WHERE username = (?)", (name,))
            update.message.reply_text("board member removed")

        self.database_conn.commit()

    def add_board_member(self, update, context):
        if context.args == []:
            update.message.reply_text("Usage: /add 'name'")
        else:
            name = context.args[0].lower()

            if self.database_cursor.execute("SELECT * FROM taartpunt WHERE username = (?)", (name,)).fetchone():
                update.message.reply_text("Board member already exists")
            else:
                self.database_cursor.execute("INSERT INTO taartpunt VALUES ((?), 0)", (name,))
                update.message.reply_text("New board member added")

        self.database_conn.commit()

    def list_board_members(self, update, _):
        text = "== Board members == \n"

        for row in self.database_cursor.execute("SELECT * FROM taartpunt"):
            print(row)
            text += row[0] + " \n"

        update.message.reply_text(text)

    def add_taartpunt(self, update, context):
        if context.args == []:
            update.message.reply_text("Usage: /taartpunt 'name' 'amount'")
        else:
            name = context.args[0].lower()
            amount = int(context.args[1].lower())

            if amount < 1:
                update.message.reply_text("Amount of taartpunten should be 1 or higher!")
                return

            self.database_cursor.execute("UPDATE taartpunt SET taartpunten = taartpunten + (?) WHERE username = (?)", (amount, name))
            update.message.reply_text("Taartpunten added")

        self.database_conn.commit()

    def remove_taartpunt(self, update, context):
        if context.args == []:
            update.message.reply_text("Usage: /taart 'name' 'amount'")
        else:
            name = context.args[0].lower()
            amount = int(context.args[1].lower())

            if amount < 1:
                update.message.reply_text("Amount of taartpunten should be 1 or higher!")
                return

            self.database_cursor.execute("UPDATE taartpunt SET taartpunten = taartpunten - (?) WHERE username = (?)", (amount, name))
            update.message.reply_text("Taartpunten removed")

        self.database_conn.commit()

    def overview(self, update, _):
        text = "== Achieved Taartpunten Week " + str(datetime.date.today().isocalendar()[1]) + " == \n"

        for row in self.database_cursor.execute("SELECT * FROM taartpunt"):
            text += row[0] + " " + str(row[1]) +" \n"

        update.message.reply_text(text)

    def notification(self, update, _):
        time = datetime.time(10, 00, 00, 000000)
        update.message.reply_text("Notification set for BV")
        self.job_queue.run_daily(self.send_overview, time, days=[Days.THU], context=update.message.chat_id)

    def send_overview(self, context):
        text = "== Overview achieved Taartpunten Week " + str(datetime.date.today().isocalendar()[1]) + " == \n"

        for row in self.database_cursor.execute("SELECT * FROM taartpunt"):
            text += row[0] + " " + str(row[1]) + " \n"

        text += "\nAll taartpunt counters set to 0 for next week.\nenjoy BV <3"
        self.dispatcher.bot.send_message(context.job.context, text)
        self.database_cursor.execute("UPDATE taartpunt SET taartpunten = 0")
        self.database_conn.commit()

    def run(self):
        # self.updater.start_webhook(listen='127.0.0.1', port=5000, url_path=self.token)
        # self.updater.bot.set_webhook(webhook_url="https://jorantimmerman.nl/" + self.token)
        self.updater.start_polling()
        self.updater.idle()


def main():
    # Token is in environment file
    load_dotenv("telegram.env")
    token = os.getenv("TELEGRAM_BOT_TOKEN")

    bot = TaartPuntenBot(token)
    bot.run()


if __name__ == '__main__':
    main()
